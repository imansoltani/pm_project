import os
import re
import time
import csv
import pandas as pd
import datetime
from collections import defaultdict
import snakes.plugins
snakes.plugins.load('gv', 'snakes.nets', 'nets')
from nets import *

start = time.time()
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin'

def CheckChoice(ChoiceList,set1,set2):
    for i in range(len(set1)):
        for j in range(len(set2)):
            k = {list(set1)[i],list(set2)[j]}
            if (not k in ChoiceList):
                return False
    return True

Logfootprint = defaultdict(dict)
Logfinal_footprint = defaultdict(dict)

data = pd.read_csv('./Dataset/sample.csv',encoding="utf8", delimiter=',')
data['EVENTTIME'] =pd.to_datetime(data.EVENTTIME)
data = data.sort_values(by=['Case ID','EVENTTIME'],ignore_index=True)
Activities = data['ACTIVITY'].unique()
Activities.sort()

for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(len(Activities)):
        colch=chr(ord('a') + j)
        Logfootprint[rowch][colch]=''
        Logfinal_footprint[rowch][colch]=''
       
CaseIDs = data['Case ID'].unique()
CaseActivities = [['' for i in range(2)] for j in range (len(CaseIDs))]

dic = {}
dic2 = {}
for i in range(len(Activities)):
    ch=chr(ord('a') + i) 
    dic [Activities[i]]=ch
    dic2 [ch] =Activities[i]
ii=0
for i in range(len(data['ACTIVITY'])):
    data['NewName'][i]=dic[data['ACTIVITY'][i]]
    if CaseActivities[ii][0] != data['Case ID'][i] and i != 0:
        ii=ii+1
    CaseActivities[ii][0] = data['Case ID'][i]
    if CaseActivities[ii][1] != '' :
        CaseActivities[ii][1] =CaseActivities[ii][1] + ',' + data['NewName'][i]
    else:
        CaseActivities[ii][1] =data['NewName'][i]
#data.to_csv('./Dataset/ConvertedP2P.csv',encoding="utf8")

for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(len(Activities)):
        colch=chr(ord('a') + j) 
        for k in range(len(CaseActivities)):
            if rowch+','+colch in CaseActivities[k][1]:
                Logfootprint[rowch][colch]='1'
                break
       


for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(Activities)):
        colch=chr(ord('a') + j) 
        if Logfootprint[rowch][colch] == '1' and Logfootprint[colch][rowch] == '1':
            Logfinal_footprint[rowch][colch] = '||'
            Logfinal_footprint[colch][rowch] = '||'
        elif Logfootprint[rowch][colch] == '1' and Logfootprint[colch][rowch] != '1':
            Logfinal_footprint[rowch][colch] = '->'
            Logfinal_footprint[colch][rowch] = '<-'
        elif Logfootprint[rowch][colch] != '1' and Logfootprint[colch][rowch] == '1':
            Logfinal_footprint[rowch][colch] = '<-'
            Logfinal_footprint[colch][rowch] = '->'
        elif Logfootprint[rowch][colch] != '1' and Logfootprint[colch][rowch] != '1':
            Logfinal_footprint[rowch][colch] = '#'
            Logfinal_footprint[colch][rowch] = '#'

ChoiceSet = [set()]

for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(Activities)):
        colch=chr(ord('a') + j) 
        if Logfinal_footprint[rowch][colch] =='#':
            ChoiceSet.append({rowch,colch})

#footprint of Model
arc = [[str for i in range (2)]]
arc.clear()
with open('./Dataset/Out.txt') as f:
    lines = f.readlines()
k=0
transition=[]
transition.clear()
for i in range(len(lines)):
    if lines[i][0] == '(' and 'End' not in  lines[i] and 'Start' not in lines[i]:
        arc.append(['' for c in range(2)])
        arc[k][0]=(re.search('\((.*)_(.*)', lines[i]).group(1))
        if arc[k][0][0] !='P':
            transition.append(arc[k][0])
        arc[k][1]=(re.search('\((.*)_(.*)\)', lines[i]).group(2))
        if arc[k][1][0] !='P':
            transition.append(arc[k][1])
        k=k+1
transition.sort()
transition = reduce(lambda l, x: l.append(x) or l if x not in l else l, transition, [])

footprint = defaultdict(dict)
final_footprint = defaultdict(dict)
for i in range(len(transition)):
    rowch=chr(ord('a') + i) 
    for j in range(len(transition)):
        colch=chr(ord('a') + j)
        footprint[rowch][colch]=''
        final_footprint[rowch][colch]=''
dic = {}
dic2 = {}
for i in range(len(transition)):
    ch=chr(ord('a') + i) 
    dic [transition[i]]=ch
    dic2 [ch] =transition[i]

for i in range(len(arc)):
    for j in range (i+1,len(arc)):
        if arc[i][1]==arc[j][0] and arc[i][1][0]== 'P':
            footprint[arc[i][0]][arc[j][1]]='1'
L2 = [[set() for i in range(2)]]
L2.clear()
for i in range(len(arc)):
    if arc[i][0][0] =='P'    :
        L2.append([set([x.strip() for x in re.search("P\({(.*)},{(.*)}\)", arc[i][0]).group(1).split(',')]),set([x.strip() for x in re.search("P\({(.*)},{(.*)}\)", arc[i][0]).group(2).split(',')])])
for i in range(len(L2)):
    for j in range(i+1,len(L2)-1):
        if L2[i][0] == L2[j][0] and  L2[i][1] == L2[j][1]:
            del L2[j]
            j=j-1
for i in range(len(L2)):
    for j in range(i+1,len(L2)):
        if L2[i][0] == L2[j][0] and  len(L2[i][1])>1 and len(L2[j][1])>1 and len(set(L2[i][1]).intersection(set(L2[j][1])))>0 :
            footprint[list(set(L2[j][1]).difference(set(L2[i][1])))[0]][list(set(L2[i][1]).difference(set(L2[j][1])))[0]]='1'
            footprint[list(set(L2[i][1]).difference(set(L2[j][1])))[0]][list(set(L2[j][1]).difference(set(L2[i][1])))[0]]='1'
        if L2[i][1] == L2[j][1] and  len(L2[i][0])>1 and len(L2[j][0])>1 and len(set(L2[i][0]).intersection(set(L2[j][0])))>0 :
            footprint[list(set(L2[j][0]).difference(set(L2[i][0])))[0]][list(set(L2[i][0]).difference(set(L2[j][0])))[0]]='1'
            footprint[list(set(L2[i][0]).difference(set(L2[j][0])))[0]][list(set(L2[j][0]).difference(set(L2[i][0])))[0]]='1'

for i in range(len(transition)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(transition)):
        colch=chr(ord('a') + j) 
        if footprint[rowch][colch] == '1' and footprint[colch][rowch] == '1':
            final_footprint[rowch][colch] = '||'
            final_footprint[colch][rowch] = '||'
        elif footprint[rowch][colch] == '1' and footprint[colch][rowch] != '1':
            final_footprint[rowch][colch] = '->'
            final_footprint[colch][rowch] = '<-'
        elif footprint[rowch][colch] != '1' and footprint[colch][rowch] == '1':
            final_footprint[rowch][colch] = '<-'
            final_footprint[colch][rowch] = '->'
        elif footprint[rowch][colch] != '1' and footprint[colch][rowch] != '1':
            final_footprint[rowch][colch] = '#'
            final_footprint[colch][rowch] = '#'

dif =0
for i in range(len(transition)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(transition)):
        colch=chr(ord('a') + j) 
        if Logfinal_footprint[rowch][colch] != final_footprint[rowch][colch]:
            dif=dif+1
dif = 1- dif/(len(final_footprint)*len(final_footprint))
print("Conformance Score = "+str(dif))
end = time.time()
print (end-start)