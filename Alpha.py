import os
import re
import time
import csv
#from snakes.nets import *
import pandas as pd
import datetime
from collections import defaultdict
import snakes.plugins
snakes.plugins.load('gv', 'snakes.nets', 'nets')
from nets import *

start = time.time()
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin'

def CheckChoice(ChoiceList,set1,set2):
    for i in range(len(set1)):
        for j in range(len(set2)):
            k = {list(set1)[i],list(set2)[j]}
            if (not k in ChoiceList):
                return False
    return True

footprint = defaultdict(dict)
final_footprint = defaultdict(dict)

data = pd.read_csv('./Dataset/sample.csv',encoding="utf8", delimiter=',')
data['EVENTTIME'] =pd.to_datetime(data.EVENTTIME)
data = data.sort_values(by=['Case ID','EVENTTIME'],ignore_index=True)
Activities = data['ACTIVITY'].unique()
Activities.sort()


for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(len(Activities)):
        colch=chr(ord('a') + j)
        footprint[rowch][colch]=''
        final_footprint[rowch][colch]=''
       
CaseIDs = data['Case ID'].unique()
CaseActivities = [['' for i in range(2)] for j in range (len(CaseIDs))]

dic = {}
dic2 = {}
for i in range(len(Activities)):
    ch=chr(ord('a') + i) 
    dic [Activities[i]]=ch
    dic2 [ch] =Activities[i]
ii=0
for i in range(len(data['ACTIVITY'])):
    data['NewName'][i]=dic[data['ACTIVITY'][i]]
    if CaseActivities[ii][0] != data['Case ID'][i] and i != 0:
        ii=ii+1
    CaseActivities[ii][0] = data['Case ID'][i]
    if CaseActivities[ii][1] != '' :
        CaseActivities[ii][1] =CaseActivities[ii][1] + ',' + data['NewName'][i]
    else:
        CaseActivities[ii][1] =data['NewName'][i]
#data.to_csv('./Dataset/ConvertedP2P.csv',encoding="utf8")

for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(len(Activities)):
        colch=chr(ord('a') + j) 
        for k in range(len(CaseActivities)):
            if rowch+','+colch in CaseActivities[k][1]:
                footprint[rowch][colch]='1'
                break
       


for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(Activities)):
        colch=chr(ord('a') + j) 
        if footprint[rowch][colch] == '1' and footprint[colch][rowch] == '1':
            final_footprint[rowch][colch] = '||'
            final_footprint[colch][rowch] = '||'
        elif footprint[rowch][colch] == '1' and footprint[colch][rowch] != '1':
            final_footprint[rowch][colch] = '->'
            final_footprint[colch][rowch] = '<-'
        elif footprint[rowch][colch] != '1' and footprint[colch][rowch] == '1':
            final_footprint[rowch][colch] = '<-'
            final_footprint[colch][rowch] = '->'
        elif footprint[rowch][colch] != '1' and footprint[colch][rowch] != '1':
            final_footprint[rowch][colch] = '#'
            final_footprint[colch][rowch] = '#'

ChoiceSet = [set()]

for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(i,len(Activities)):
        colch=chr(ord('a') + j) 
        if final_footprint[rowch][colch] =='#':
            ChoiceSet.append({rowch,colch})


FinalL2 = [[set() for i in range(2)]]
L2 = [[set() for i in range(2)]]
L2.clear()
for i in range(len(Activities)):
    rowch=chr(ord('a') + i) 
    for j in range(len(Activities)):
        colch=chr(ord('a') + j) 
        if final_footprint[rowch][colch] =='->':
            L2.append([{rowch},{colch}])

for i in range(len(L2)):
    for j in range(i+1,len(L2)):
        if (set(L2[i][0]).issubset(set(L2[j][0])) or set(L2[i][1]).issubset(set(L2[j][1]))):
            if CheckChoice(ChoiceSet,set(L2[i][0]),set(L2[j][0])) and CheckChoice(ChoiceSet,set(L2[i][1]),set(L2[j][1])):
                if not [set(L2[i][0]).union(set(L2[j][0])),set(L2[i][1]).union(set(L2[j][1]))] in L2:
                    L2.append([set(L2[i][0]).union(set(L2[j][0])),set(L2[i][1]).union(set(L2[j][1]))])
               
i=0
while i<len(L2):
    Isdelete=False
    for j in range(i+1,len(L2)):
         if (set(L2[i][0]).issubset(set(L2[j][0])) and set(L2[i][1]).issubset(set(L2[j][1]))):
             L2.remove(L2[i])
             Isdelete = True
             break
    if not Isdelete:
        i = i+1
net = PetriNet("PN")
for i in range(len(Activities)):
    net.add_transition(Transition(Activities[i]))
startTransition = set()
endTransition = set()
AllArcs = []
#Find All Start Activity and Add Arc
for i in range (len(CaseActivities)):
    startTransition.add(dic2[list(CaseActivities[i][1])[0]])
net.add_place(Place('Start'))
for i in range(len(startTransition)):
    net.add_input('Start',list(startTransition)[i], Variable('x'))
    AllArcs.append('(Start_'+str(dic[list(startTransition)[i]])+')')
#Find All End Activity and Add Arc
for i in range (len(CaseActivities)):
    endTransition.add(dic2[list(CaseActivities[i][1])[-1]])
net.add_place(Place('End'))
for i in range(len(endTransition)):
    net.add_output('End',list(endTransition)[i],'')
    AllArcs.append('('+str(dic[list(endTransition)[i]])+'_End)')


for i in range(len(L2)):
    net.add_place(Place('p_'+str(i)))
    for pl in list(L2[i][0]):
        net.add_output('p_'+str(i),dic2[pl],'')
        AllArcs.append('('+str(pl)+'_P('+str(L2[i][0])+','+str(L2[i][1])+'))')
    for pl in list(L2[i][1]):
        net.add_input('p_'+str(i),dic2[pl], Variable('l'))
        AllArcs.append('(P('+str(L2[i][0])+','+str(L2[i][1])+')_'+str(pl)+')')
for i in range(len(AllArcs)):
    AllArcs[i] = re.sub('[\']', '', AllArcs[i])

net.draw(filename='./Dataset/test.png',engine='dot')
file1 = open('./Dataset/Out.txt','w+')
file1.write('-----------Rename activity to simplify------------------------\n')

for key, value in dic.items():
    file1.write(value+':'+key +'\n')
file1.write('-----------Tha list of Arcs( F_L) in Petri net------------------------\n')
for i in range(len(AllArcs)):
    file1.writelines(AllArcs[i]+'\n')
file1.close()
end = time.time()
print (end-start)